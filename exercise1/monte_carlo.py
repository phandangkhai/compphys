import numpy as np

def monte_carlo_integration(fun,xmin,xmax,blocks=10,iters=100):
    # A function implementing monte carlo integration
    block_values=np.zeros((blocks,))
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            x = xmin+np.random.rand()*L
            block_values[block]+=fun(x)
        block_values[block]/=iters
    I = L*np.mean(block_values)
    dI = L*np.std(block_values)/np.sqrt(blocks)
    return I,dI

def func(x):
    # A test function f = sin to test numerical integration.
    return np.sin(x)

def main():
    I,dI=monte_carlo_integration(func,0.,np.pi/2,10,100)
    print('Integrated value: {0:0.5f} +/- {1:0.5f}'.format(I,2*dI))
    print('Analytical value is I = '+str(1))
    if 1>(I-2*dI) and 1<(I+2*dI):
        print('The result from monte carlo is indeed within the error margin!')
if __name__=="__main__":
    main()
