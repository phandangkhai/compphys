''' This python code computes and plots the absolute error
of first derivative approximation and simpson integration
as functions of resolution step dx.'''
import numpy as np
import matplotlib.pyplot as plt
from num_calculus import first_derivative, simpson_int


# We test convergence of first derivative function over various values of dx 
dx = np.linspace(0.001, 0.01, 10)
# variables to test first derivative: f = sin(x) at x = 1
x = 1
# boundaries of integration are (a,b)
a = 0
b = np.pi/2

# intialize the arrays used for plotting
deriv_values = np.zeros([len(dx),1])
deriv_errors = np.zeros(np.shape(deriv_values))
simpson_values = np.zeros(np.shape(deriv_values))
simpson_errors = np.zeros(np.shape(deriv_values))

for i in range(len(dx)): 
    N = round((b-a)/dx[i])
    # prepare the discrete arrays input for simpson integration function,
    # boundaries of integration are (a,b)
    # Making sure the number of bins are even for simpson rule
    if (N%2) == 1:
        N += 1
    x_arr = np.linspace(0, np.pi/2, N-1)
    f_arr = np.sin(x_arr)

    deriv_values[i] = first_derivative(np.sin,x,dx[i])
    deriv_errors[i] = np.abs(deriv_values[i] - np.cos(x))
    simpson_values[i] = simpson_int(f_arr, x_arr)
    simpson_errors[i] = np.abs(simpson_values[i] - 1)



# Plotting
plt.rcParams['legend.handlelength'] = 2
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['font.size'] = 12
fig = plt.figure()
# - or, e.g., fig = plt.figure(figsize=(width, height))
# - so-called golden ratio: width=height*(np.sqrt(5.0)+1.0)/2.0
# - width and height are given in inches (1 inch is roughly 2.54 cm)
ax = fig.add_subplot(111)
# plot and add label if legend desired
ax.plot(dx,deriv_errors,label=r'First derivative')
ax.plot(dx,simpson_errors,'--',label=r'Simpson integration')
# include legend (with best location, i.e., loc=0)
ax.legend(loc=0)
# set axes labels and limits
ax.set_xlabel(r'dx')
ax.set_ylabel(r'error(dx)')
ax.set_xlim(dx.min(), dx.max())
fig.tight_layout(pad=1)
# save figure as pdf with 200dpi resolution
fig.savefig('testfile.pdf',dpi=200)
plt.show()
