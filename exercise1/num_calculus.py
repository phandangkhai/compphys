""" This python file includes implementations of 
first/second derivative approximation and simpson integration functions 
and the corresponding tests """
# import needed packages, e.g., import numpy as np
import numpy as np
import matplotlib.pyplot as plt

def first_derivative( f, x, dx ):
    # Foward first derivative approximation
    return (f(x+dx)- f(x))/dx

def test_first_derivative():
    # A simple test by comparing the analytical value and the computed value of 
    # the first derivative of cos(x) at x = 1.
    result = first_derivative(np.cos, 1, 0.001)
    answer = -np.sin(1)
    print("The correct answer is f'(1) = "+str(answer)+", where f(x) = cos(x)")
    print("The result from first_derivative is "+str(result)+", step h = 0.001")

def second_derivative( f, x, dx ):
    # Discrete approximation of second derivative
    return (f(x+dx)+f(x-dx)-2*f(x))/(dx**2)

def test_second_derivative():
    # A test similar to the test_first_derivative().
    result = second_derivative(np.cos, 1, 0.001)
    answer = -np.cos(1)
    print("The correct answer is f''(1) = "+str(answer)+", where f(x) = cos(x)") 
    print("The result from second_derivative is "+str(result)+", step h = 0.001")

def simpson_int(f ,x):
    # simpson integration where inputs are
    # x: array of discrete values
    # f: array of discrete values of the function f(x)
    dx = (x[-1]-x[0])/(len(x)+1)
    if (len(x)+1) % 2 == 1:
        raise ValueError("Array length has to be even!")
    S = dx/3 * np.sum(f[0:-1:2] + 4*f[1::2] + f[2::2])
    return S

def main():
    # The main function calls the tests for first and second derivative 
    # as well as executing a test for simpson integration.
    test_first_derivative()
    test_second_derivative()
    x = np.linspace(0, np.pi/2, 99) # number of bins N = 100.
    f = np.sin(x)
    I = simpson_int(f,x)
    print("result for simpson_int I = " + str(I))

if __name__=="__main__":
    main()
